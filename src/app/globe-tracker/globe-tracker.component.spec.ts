import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobeTrackerComponent } from './globe-tracker.component';

describe('GlobeTrackerComponent', () => {
  let component: GlobeTrackerComponent;
  let fixture: ComponentFixture<GlobeTrackerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GlobeTrackerComponent]
    });
    fixture = TestBed.createComponent(GlobeTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
