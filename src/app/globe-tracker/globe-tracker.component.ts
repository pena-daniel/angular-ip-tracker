import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { debounceTime, distinctUntilChanged, fromEvent, map } from 'rxjs';
import TrackballControls from 'three-trackballcontrols';
import {CSS2DRenderer,CSS2DObject} from 'three-css2drenderer';
import ThreeGlobe from 'three-globe';
import * as THREE from 'three';

@Component({
  selector: 'app-globe-tracker',
  templateUrl: './globe-tracker.component.html',
  styleUrls: ['./globe-tracker.component.scss']
})
export class GlobeTrackerComponent implements OnInit{

  ipAddress:string = 'Chargement...'
  data: any = {}
  loading:boolean = false
  @ViewChild('ipInput', { static: true }) ipInput!: ElementRef;
  globe!:ThreeGlobe;
  renderer!:THREE.WebGLRenderer[];
  scene !:THREE.Scene;
  camera !:THREE.PerspectiveCamera;
  tbControls !:typeof TrackballControls;
  imageLink = "https://raw.githubusercontent.com/vasturiano/three-globe/master/example/img/earth-day.jpg"
  markerSvg = `<svg viewBox="-4 0 36 36">
      <path fill="currentColor" d="M14,0 C21.732,0 28,5.641 28,12.6 C28,23.963 14,36 14,36 C14,36 0,24.064 0,12.6 C0,5.641 6.268,0 14,0 Z"></path>
      <circle fill="black" cx="14" cy="14" r="7"></circle>
    </svg>`;


  constructor(private http: HttpClient){}


  ngOnInit(){
    this.getCurrentIp()
    fromEvent(this.ipInput.nativeElement, 'keyup').pipe(
      map((event: any) => {
        return event.target.value;
      }),
      // Time in milliseconds between key events
      debounceTime(500)

      // If previous query is diffent from current
      , distinctUntilChanged()

      // subscription for response
    ).subscribe((text: string) => {
      this.getLatLongPosition(text)
    });

    this.initGlobe();
  }

  initGlobe():void{
    this.globe = new ThreeGlobe().globeImageUrl(this.imageLink)
    .bumpImageUrl('//unpkg.com/three-globe/example/img/earth-topology.png')
    .pointAltitude('size')
    .pointColor('color');
    this.setRederer();
    this.setScene();
    this.setCamera();
    this.setControls();
    this.animate();
    this.cameraView();
  }

  getCurrentIp():void{
    this.ipAddress = "Chargement..."
    this.data = {}
    this.http
    .get<any>(`https://api.bigdatacloud.net/data/client-ip`, {})
    .subscribe(
      (data) => {
        this.ipAddress = ""
        if (data.ipString) {
          this.ipAddress = data.ipString
          this.getLatLongPosition(this.ipAddress)
        }
        else{
          alert('connexion error');
          this.ipAddress = ""
          this.data.error = ''
        }
      }
    );
  }

  getLatLongPosition(ip:string):void{
    this.data = {}
    this.loading = true
    this.http
    .get<any>(`https://api.techniknews.net/ipgeo/`+ip, {})
    .subscribe(
      (data) => {
        this.loading = false
        if(data.lon) {
          this.data = data
          this.setPoint(data.lon,data.lat)
        }
        else {
          this.data.error = ''
          alert('error in your ip adrress check again');
          this.getCurrentIp();
        }
      }
    );
  }

  setPoint(lng:string, lat:string):void{
    this.globe.pointsData([{
      lat,
      lng,
      size: .5,
      color: 'red'
    }])
    let p = this.globe.getCoords(+lat, +lng, 2)
    this.camera.updateProjectionMatrix();
    this.camera.position.setZ(p.z).setY(p.y).setX(p.x)

  }

  setScene():void{
    this.scene = new THREE.Scene();
    this.scene.add(this.globe);
    this.scene.add(new THREE.AmbientLight(0xcccccc));
    this.scene.add(new THREE.DirectionalLight(0xffffff, 0.6));
  }

  setRederer():void{
    this.renderer = [new THREE.WebGLRenderer(),  new CSS2DRenderer()];
    this.renderer.forEach((r, idx) => {
      r.setSize(500, 500, true);
      if (idx > 0) {
        // overlay additional on top of main renderer
        r.domElement.style.position = 'absolute';
        r.domElement.style.top = '0px';
        r.domElement.style.pointerEvents = 'none';
      }
      document.getElementById('globe')!.appendChild(r.domElement);
    });
  }
  setCamera():void{
    this.camera = new THREE.PerspectiveCamera();
    this.camera.aspect = 1;
    this.camera.updateProjectionMatrix();
    this.camera.position.z = 500;
  }

  animate():void{
    this.tbControls.update();
    this.renderer.forEach(r => r.render(this.scene, this.camera));
    requestAnimationFrame(this.animate.bind(this));
  }
  setControls():void{
    this.tbControls = new TrackballControls(this.camera, this.renderer[0].domElement);
    this.tbControls.minDistance = 125;
    this.tbControls.rotateSpeed = 2;
    this.tbControls.zoomSpeed = 0.8;
  }

  cameraView():void{
    this.globe.setPointOfView(this.camera.position, this.globe.position);
    this.tbControls.addEventListener('change', () => this.globe.setPointOfView(this.camera.position, this.globe.position));
  }

}
