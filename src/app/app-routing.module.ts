import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GlobeTrackerComponent } from './globe-tracker/globe-tracker.component';

const routes: Routes = [
  {
    path: '',
    component: GlobeTrackerComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
